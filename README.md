# GrunLab Argo Worflows

Argo Worflows deployment on Kubernetes.

Docs: https://docs.grunlab.net/install/argo-workflows.md

GrunLab project(s) using this service:
- [grunlab/aria2][aria2]

[aria2]: <https://gitlab.com/grunlab/aria2>